import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HeaderComponent } from './components/header/header.component';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { HomeComponent } from './components/home/home.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AuthInterceptor } from './interceptors/auth-interceptor';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SingleProductViewComponent } from './recipes/single-product-view/single-product-view.component';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmModalComponent } from './shared/confirm-modal/confirm-modal.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { ServerErrorInterceptor } from './interceptors/401-interceptor';
// import { SpinnerInterceptor } from './interceptors/spinner-interceptor';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    HomeComponent,
    SingleProductViewComponent,
    NotFoundComponent,
  ],
  imports: [
    SharedModule,
    CoreModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    NgbPaginationModule,
    HttpClientModule,
    NgxSpinnerModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: SpinnerInterceptor,
    //   multi: true
    // }
  ],
  bootstrap: [AppComponent],
  entryComponents: [SingleProductViewComponent, ConfirmModalComponent]
})
export class AppModule { }
