export enum recipeType {
 salad = '../../../assets/salad.jpeg',
 dessert = '../../../assets/dessert.jpeg',
 garnish = '../../../assets/garnish.jpeg',
 sauce = '../../../assets/sauce.jpeg',
 protein = '../../../assets/protein.jpeg',
 meal = '../../../assets/meal.jpeg',
}
