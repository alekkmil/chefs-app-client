import { Nutrient } from './nutrient';

export interface Ingredient {
    productDescription: string;
    productCode: number;
    measure: string;
    amount: number;
    nutrients: Nutrient[];
}
