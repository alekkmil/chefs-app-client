import { Measure } from './measure';
import { Nutrient } from './nutrient';
import { Nutrition } from './nutrition';
export interface Product {
    code: number;
    description: string;
    // foodGroup: foodGroup;
    measures: Measure[];
    nutrition: Nutrition;
}
