export interface CreateIngredient {
    productCode: number;
    amount: number;
}
