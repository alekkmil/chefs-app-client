import { Product } from './product';

export interface QueryProducts {
  products: Product [];
  totalProducts: number;
}
