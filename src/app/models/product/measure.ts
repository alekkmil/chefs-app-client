export interface Measure {
    measure: string;
    gramsPerMeasure: number;
    id: string;
}
