import { Nutrient } from '../product/nutrient';

export interface RecipeNutritionInfo {
    PROCNT: Nutrient;
    FAT: Nutrient;
    CHOCDF: Nutrient;
    ENERC_KCAL: Nutrient;
    NA: Nutrient;
    FE: Nutrient;
    SUGAR: Nutrient;
    FIBTG: Nutrient;
    P: Nutrient;
    VITD: Nutrient;
}
