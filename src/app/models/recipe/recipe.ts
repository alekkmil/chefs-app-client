import { Ingredient } from '../product/ingredient';
import { RecipeNutritionInfo } from './recipeSmallNutritionInfo';

export interface Recipe {
    id: string;
    title: string;
    type: string;
    parentRecipes?: string[];
    ingredients: Ingredient[];
    recipeNutrition: RecipeNutritionInfo;
    image: string;
}
