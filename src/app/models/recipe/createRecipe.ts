import { CreateIngredient } from '../product/createIngredient';

export interface CreateRecipe {
    title: string;
    recipeType: string;
    ingridients: CreateIngredient[];
}
