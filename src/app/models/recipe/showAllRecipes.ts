import { RecipeNutritionInfo } from './recipeSmallNutritionInfo';
import { Recipe } from './recipe';

export interface ShowAllRecipes {
    id: string;
    type: string;
    title: string;
    recipeNutrition: RecipeNutritionInfo;
    date?: Date;
    parentRecipes?: string[];
    subRecipes?: Recipe[];
    image: string;
}
