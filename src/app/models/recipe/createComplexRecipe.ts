import { CreateIngredient } from '../product/createIngredient';

export class CreateComplexRecipe {
    title: string;
    subRecipes: string[];
    ingridients: CreateIngredient[];
}
