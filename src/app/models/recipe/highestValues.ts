export interface HighestValues {
    ENERC_KCAL: number;
    NA: number;
    FAT: number;
    PROCNT: number;
    CHOCDF: number;
}
