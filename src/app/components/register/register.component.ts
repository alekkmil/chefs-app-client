import { Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { UserLoginInfo } from 'src/app/models/user/userLoginInfo';
import { Router } from '@angular/router';
import { NotificatorService } from 'src/app/core/services/notificator/notificator.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public registerForm: FormGroup;
  public username: AbstractControl;
  public password: AbstractControl;

  constructor(
    private readonly authService: AuthService,
    private readonly formBuilder: FormBuilder,
    private readonly router: Router,
    private readonly notificator: NotificatorService,
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(5)]],
      password: ['', [Validators.required, Validators.minLength(3)]],
    });
    this.username = this.registerForm.controls.username;
    this.password = this.registerForm.controls.password;
  }

  public register() {
    const user: UserLoginInfo = this.registerForm.value;
    this.authService.register(user).subscribe(
      () => {
        this.notificator.success('You have registered !');
        this.router.navigate(['/login']);
      },
      (error) => {
        this.notificator.error(error.error.message);
      }
    );
  }

}
