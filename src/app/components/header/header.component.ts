import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { User } from 'src/app/models/user/user';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { async } from 'q';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit , OnDestroy {

  public isLogged: boolean;
  public user: User;
  public userSubjectSubscription: Subscription;

  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
  ) { }

  ngOnInit() {
    this.userSubjectSubscription = this.authService.userSubject$.subscribe( (user) => {
      if (user) {
        this.isLogged = true;
        this.user = user;
      } else {
        this.isLogged = false;
      }
    });
  }
  public logout() {
    this.authService.logout().subscribe(
      () => {
        this.router.navigate(['/login']);
      },
      (error) => {
        console.log(error);
      },
    );
  }



  ngOnDestroy() {
    this.userSubjectSubscription.unsubscribe();
  }

}
