import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { UserLoginInfo } from 'src/app/models/user/userLoginInfo';
import { NotificatorService } from 'src/app/core/services/notificator/notificator.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  public loginForm: FormGroup;
  public username: AbstractControl;
  public password: AbstractControl;
  private loginSubscription: Subscription;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly notificator: NotificatorService,
  ) { }

  ngOnInit() {
    this.loginSubscription = this.authService.userSubject$.subscribe( user => {
      if (user) {
        this.router.navigate(['recipes']);
      }
    });
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(5)]],
      password: ['', Validators.required]
    });
    this.username = this.loginForm.controls.username;
    this.password = this.loginForm.controls.password;
  }
  public login() {
    const user: UserLoginInfo = this.loginForm.value;
    this.authService.login(user).subscribe(
      () => {
        this.notificator.success('Successful login !');
        this.router.navigate(['/recipes']);
      },
      (error) => {
        this.notificator.error(error.error.message);
      }

    );
  }

  ngOnDestroy() {
    this.loginSubscription.unsubscribe();
  }



}
