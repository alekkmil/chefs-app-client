import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NotificatorService } from '../core/services/notificator/notificator.service';
import { AuthService } from '../core/services/auth/auth.service';

@Injectable()
export class ServerErrorInterceptor implements HttpInterceptor {
  public constructor(
    private readonly router: Router,
    private readonly notificator: NotificatorService,
    private readonly authService: AuthService,
  ) {}
  public intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((error: any) => {
         if (error.status === 401) {
          this.authService.logout();
          this.notificator.error(`You're unauthorized to access this page! Please login!`);
          this.router.navigate(['login']);
         } else if (error.status === 404) {
             this.router.navigate(['not-found']);
         }
         return throwError(error);
      })
    );
  }
}
