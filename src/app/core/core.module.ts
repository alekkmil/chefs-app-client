import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthService } from './services/auth/auth.service';
import { NotificatorService } from './services/notificator/notificator.service';
import { ToastrModule } from 'ngx-toastr';
import { ProductsService } from './services/products/products.service';
import { RecipeService } from './services/recipe/recipe.service';
import { StorageService } from './services/storage/storage.service';


@NgModule({
  providers: [AuthService, NotificatorService, ProductsService, RecipeService , StorageService],
  declarations: [],
  imports: [
    CommonModule,
    ToastrModule.forRoot(),
  ]
})
export class CoreModule { }
