import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from 'src/app/models/user/user';
import { StorageService } from '../storage/storage.service';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private userInfoSubject$ = new BehaviorSubject<User | undefined>(this.isUserAuthenticated());

  constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
    private readonly router: Router,
  ) {}

  public get userSubject$(): Observable<User | undefined> {
    return this.userInfoSubject$.asObservable();
  }

  public register(user: any): Observable<any> {
    return this.http.post('http://localhost:3000/register', user);
  }

  public login(user: any): Observable<any> {
    return this.http.post('http://localhost:3000/login', user).pipe(
      tap(data => {
        this.storage.setItem('token' , data.token);
        this.storage.setItem('username' , user.username);
        const userInfo: User = {
          username: data.username,
          recipes: data.recipes,
        };
        this.userInfoSubject$.next(userInfo);
      })
    );
  }
  public logout(): Observable<any> {
    return this.http.delete('http://localhost:3000/logout').pipe(
      tap(() => {
        this.storage.removeItem('token');
        this.storage.removeItem('username');
        this.userInfoSubject$.next(undefined);
        this.router.navigate(['login']);
      })
    );
  }

  private isUserAuthenticated() {
    if (this.storage.getItem('token') === null || '') {
      return undefined;
    } else {
      const username = this.storage.getItem('username');
      this.http.get<User>(`http://localhost:3000/users/${username}`).subscribe( (user) => {
        return this.userInfoSubject$.next(user);
      });
     }
    }
}
