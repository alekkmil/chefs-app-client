import { FoodGroup } from './../../../models/product/food-group';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from 'src/app/models/product/product';
import { HttpClient } from '@angular/common/http';
import { QueryProducts } from './../../../../app/models/product/query-products';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(
    private readonly http: HttpClient
  ) { }

  public getProducts(page: number = 1, searchString: string = '', foodGroupCode: number = 0): Observable<{ items: Product[] }> {
    // tslint:disable-next-line:max-line-length
    return this.http.get<{ items: Product[] }>(`http://localhost:3000/products?page=${page}&limit=10&food=${foodGroupCode}&string=${searchString}`);
  }

  public getQueryProducts(page: number = 1, searchString: string = '', foodGroupCode: number = 0): Observable<QueryProducts> {

    // tslint:disable-next-line:max-line-length
    return this.http.get<QueryProducts>(`http://localhost:3000/products/query/?page=${page}&limit=10&food=${foodGroupCode}&string=${searchString}`);
  }

  public getAllProductsNumber(): Observable<number> {
    return this.http.get<number>('http://localhost:3000/products/count');
  }
  public getAllFoodGroups(): Observable<FoodGroup[]> {
    return this.http.get<FoodGroup[]>('http://localhost:3000/products/groups');
  }
  public getASingleProduct(productCode: number): Observable<Product> {
    return this.http.get<Product>(`http://localhost:3000/products/${productCode}`);
  }
}
