import { Injectable } from '@angular/core';
import { Product } from 'src/app/models/product/product';
import { Ingredient } from 'src/app/models/product/ingredient';
import { Nutrient } from 'src/app/models/product/nutrient';
import { Nutrition } from 'src/app/models/product/nutrition';
import { CreateIngredient } from 'src/app/models/product/createIngredient';

@Injectable({
  providedIn: 'root'
})
export class ProductsHelperService {

  constructor() { }

  public createIngredients(product: Product , amount: number , measure: string): Ingredient {
    const nutrition: Nutrition = product.nutrition;
    const ingredientToadd: Ingredient = {
      productDescription: product.description,
      productCode: product.code,
      amount,
      measure,
      nutrients: [{
          description: nutrition.CA.description,
          unit: nutrition.CA.unit,
          value: (nutrition.CA.value * amount) / 100,
      }, {
          description: nutrition.FAT.description,
          unit: nutrition.FAT.unit,
          value: (nutrition.FAT.value * amount) / 100,
      }, {
          description: nutrition.PROCNT.description,
          unit: nutrition.PROCNT.unit,
          value: (nutrition.PROCNT.value * amount) / 100,
      }, {
          description: nutrition.CHOCDF.description,
          unit: nutrition.CHOCDF.unit,
          value: (nutrition.CHOCDF.value * amount) / 100,
      }, {
          description: nutrition.ENERC_KCAL.description,
          unit: nutrition.ENERC_KCAL.unit,
          value: (nutrition.ENERC_KCAL.value * amount) / 100,
      }, {
          description: nutrition.SUGAR.description,
          unit: nutrition.SUGAR.unit,
          value: (nutrition.SUGAR.value * amount) / 100,
      }, {
          description: nutrition.NA.description,
          unit: nutrition.NA.unit,
          value: (nutrition.NA.value * amount) / 100,
      }, {
          description: nutrition.FIBTG.description,
          unit: nutrition.FIBTG.unit,
          value: (nutrition.FIBTG.value * amount) / 100,
      }, {
          description: nutrition.FE.description,
          unit: nutrition.FE.unit,
          value: (nutrition.FE.value * amount) / 100,
      }, {
          description: nutrition.CHOLE.description,
          unit: nutrition.CHOLE.unit,
          value: (nutrition.CHOLE.value * amount) / 100,
      },
     ],
    };
    return ingredientToadd;
  }

  public convertIngredients(ingredients: Ingredient[]): CreateIngredient[] {
    const ingredientsToReturn: CreateIngredient[] = ingredients.map( ingr => {
        const ingrToreturn: CreateIngredient = {
            productCode: ingr.productCode,
            amount: ingr.amount,
          };
        return ingrToreturn;
    });
    return ingredientsToReturn;
  }
}
