import { TestBed } from '@angular/core/testing';

import { ProductsHelperService } from './products-helper.service';

describe('ProductsHelperService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductsHelperService = TestBed.get(ProductsHelperService);
    expect(service).toBeTruthy();
  });
});
