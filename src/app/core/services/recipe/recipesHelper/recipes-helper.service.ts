import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CreateIngredient } from 'src/app/models/product/createIngredient';
import { CreateRecipe } from 'src/app/models/recipe/createRecipe';
import { Recipe } from 'src/app/models/recipe/recipe';
import { ProductsHelperService } from '../../products/productsHelper/products-helper.service';
import { BehaviorSubject } from 'rxjs';
import { Ingredient } from 'src/app/models/product/ingredient';
import { RecipeNutritionInfo } from 'src/app/models/recipe/recipeSmallNutritionInfo';
import { CreateComplexRecipe } from 'src/app/models/recipe/createComplexRecipe';
import { ShowAllRecipes } from 'src/app/models/recipe/showAllRecipes';
import { HighestValues } from 'src/app/models/recipe/highestValues';
import { recipeType } from 'src/app/models/common/recipeType';

@Injectable({
  providedIn: 'root'
})
export class RecipesHelperService {

  constructor() { }

  public createRecipeTransform(title: string,
                               type: string,
                               ingridients: CreateIngredient[]
  ): CreateRecipe {
    const recipeToReturn: CreateRecipe = {
      title,
      recipeType: type,
      ingridients,
    };
    return recipeToReturn;
  }

  public appendImage(recipes: Array<ShowAllRecipes|Recipe>): ShowAllRecipes[] | Recipe[] {
    const recipesToReturn: ShowAllRecipes[]|Recipe[] = recipes.map(recipe => {
      recipe.image = recipeType[recipe.type];
      return recipe;
    });
    return recipesToReturn;
  }

  public calculateRecipeNutritions(ingridients: Ingredient[]): RecipeNutritionInfo {
    const nutrients = ingridients.map(ing => ing.nutrients);
    const recipeNutrition = {
      CHOCDF: {
        description: 'Carbohydrates',
        unit: 'g',
        value: 0,
      },
      FAT: {
        description: 'Total fat',
        unit: 'g',
        value: 0,
      },
      PROCNT: {
        description: 'Protein',
        unit: 'g',
        value: 0,
      },
      ENERC_KCAL: {
        description: 'Energy',
        unit: 'kcal',
        value: 0,
      },
      NA: {
        description: 'Sodium',
        unit: 'mg',
        value: 0,
      },
      SUGAR: {
        description: 'Sugars, total',
        unit: 'g',
        value: 0,
      },
      FIBTG: {
        description: 'Fiber, total dietary',
        unit: 'g',
        value: 0,
      },
      FE: {
        description: 'Iron, Fe',
        unit: 'g',
        value: 0,
      },
      P: {
        description: 'Calcium, Ca',
        unit: 'mg',
        value: 0,
      },
      VITD: {
        description: 'Vitamin D',
        unit: 'mg',
        value: 0,
      },
    };
    nutrients.map(nuts => nuts.map(nut => {
      switch (nut.description) {
        case 'Protein':
          recipeNutrition.PROCNT.value += nut.value;
          break;
        case 'Total lipid (fat)':
          recipeNutrition.FAT.value += nut.value;
          break;
        case 'Carbohydrate, by difference':
          recipeNutrition.CHOCDF.value += nut.value;
          break;
        case 'Energy':
          recipeNutrition.ENERC_KCAL.value += nut.value;
          break;
        case 'Sodium, Na':
          recipeNutrition.NA.value += nut.value;
          break;
        case 'Sugars, total':
          recipeNutrition.SUGAR.value += nut.value;
          break;
        case 'Fiber, total dietary':
          recipeNutrition.FIBTG.value += nut.value;
          break;
        case 'Iron, Fe':
          recipeNutrition.FE.value += nut.value;
          break;
        case 'Calcium, Ca':
          recipeNutrition.P.value += nut.value;
          break;
        case 'Vitamin D':
          recipeNutrition.VITD.value += nut.value;
          break;
      }
    }));
    return recipeNutrition;
  }

  public createComplexRecipe(title: string, subRecipes: Recipe[], ingridients: CreateIngredient[]): CreateComplexRecipe {
    const subRecipeIds = subRecipes.map(recipe => recipe.id);
    const recipeToReturn: CreateComplexRecipe = {
      ingridients,
      title,
      subRecipes: subRecipeIds,
    };
    return recipeToReturn;
  }
  public getMaxNutrValues(recipes: ShowAllRecipes[]): HighestValues {
    const recipeNutrition: RecipeNutritionInfo[] = recipes.map( r => r.recipeNutrition);
    const recipeCalories = recipeNutrition.map( nut => nut.ENERC_KCAL.value);
    const maxCalories = Math.max(...recipeCalories).toFixed(0);
    const recipeSalt = recipeNutrition.map( nut => nut.NA.value);
    const maxSalt = Math.max(...recipeSalt).toFixed(0);
    const recipeFat = recipeNutrition.map( nut => nut.FAT.value);
    const maxFat = Math.max(...recipeFat).toFixed(0);
    const recipeProtein = recipeNutrition.map( nut => nut.PROCNT.value);
    const maxProtein = Math.max(...recipeProtein).toFixed(0);
    const recipeCarbs = recipeNutrition.map( nut => nut.CHOCDF.value);
    const maxCarbs = Math.max(...recipeCarbs).toFixed(0);

    const maxValues: HighestValues = {
      ENERC_KCAL: +maxCalories,
      NA: +maxSalt,
      FAT: +maxFat,
      PROCNT: +maxProtein,
      CHOCDF: +maxCarbs,
    };
    return maxValues;
  }
}

