import { TestBed } from '@angular/core/testing';

import { RecipesHelperService } from './recipes-helper.service';

describe('RecipesHelperService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RecipesHelperService = TestBed.get(RecipesHelperService);
    expect(service).toBeTruthy();
  });
});
