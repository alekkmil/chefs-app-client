import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Recipe } from 'src/app/models/recipe/recipe';
import { CreateRecipe } from 'src/app/models/recipe/createRecipe';
import { Observable } from 'rxjs';
import { CreateComplexRecipe } from 'src/app/models/recipe/createComplexRecipe';
import { ShowAllRecipes } from 'src/app/models/recipe/showAllRecipes';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {

  constructor(
    private readonly http: HttpClient
  ) { }

  public getUsersRecipes(): Observable<{recipes: ShowAllRecipes[]}> {
    return this.http.get<{recipes: ShowAllRecipes[]}>('http://localhost:3000/recipes');
  }

  public getSingleRecipe(recipeId: string): Observable<Recipe> {
    return this.http.get<Recipe>(`http://localhost:3000/recipes/${recipeId}`);
  }

  public createRecipe(recipeToCreate: CreateRecipe): Observable<Recipe> {
    return this.http.post<Recipe>('http://localhost:3000/recipes' , recipeToCreate);
  }

  public createComplexRecipe(recipeToCreate: CreateComplexRecipe): Observable<Recipe> {
    return this.http.post<Recipe>('http://localhost:3000/meals' , recipeToCreate);
  }

  public getAllMeals(): Observable<{meals: ShowAllRecipes[]}> {
    return this.http.get<{meals: ShowAllRecipes[]}>('http://localhost:3000/meals');
  }
  public getSingleMeal(mealId: string) {
    return this.http.get(`http://localhost:3000/meals/${mealId}`);
  }

  public deleteRecipe(recipeId: string, recipeType: string) {
    if (recipeType === 'meal') {
      return this.http.delete(`http://localhost:3000/meals/${recipeId}`);
    } else {
    return this.http.delete(`http://localhost:3000/recipes/${recipeId}`);
    }
  }

  public updateRecipe(recipeId: string, recipeType: string, recipe: CreateRecipe): Observable<Recipe> {
    return this.http.put<Recipe>(`http://localhost:3000/recipes/${recipeId}`, recipe);
  }

  public updateMeal(recipeId: string, recipe: CreateComplexRecipe) {
    return this.http.put<Recipe>(`http://localhost:3000/meals/${recipeId}`, recipe);
  }

}
