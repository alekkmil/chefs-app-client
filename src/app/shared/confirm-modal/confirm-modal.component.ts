import { Component, Output, EventEmitter, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { RecipeService } from 'src/app/core/services/recipe/recipe.service';
import { NotificatorService } from 'src/app/core/services/notificator/notificator.service';
import { ShowAllRecipes } from 'src/app/models/recipe/showAllRecipes';
@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html'
})
export class ConfirmModalComponent implements OnInit {
  @Input() id: string;
  @Input() recipeType: string;


  constructor(
     public activeModal: NgbActiveModal,
     private readonly recipeService: RecipeService,
     private readonly notificator: NotificatorService,
     ) { }

  ngOnInit() {
    if (this.recipeType !== 'meal') {
      this.recipeService.getSingleRecipe(this.id).subscribe( (data: ShowAllRecipes) => {
        const parentRecipes = data.parentRecipes.filter(recipe => recipe !== null);
        if (parentRecipes.length > 0) {
          const recipeNames = parentRecipes.join(', ');
          this.notificator.warn(`Deleting this will affect ${recipeNames}`);
        }
      });
    }
  }

  public submit() {
    this.activeModal.close(this.id);
  }
}
