import { Component, OnInit, SimpleChanges, OnChanges, SimpleChange, OnDestroy } from '@angular/core';
import { ProductsService } from 'src/app/core/services/products/products.service';
import { Product } from 'src/app/models/product/product';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AllProductsViewComponent } from '../all-products-view/all-products-view.component';
import { Ingredient } from 'src/app/models/product/ingredient';
import { ProductsHelperService } from 'src/app/core/services/products/productsHelper/products-helper.service';
import { FormBuilder, FormGroup, AbstractControl, FormControl, Validators } from '@angular/forms';
import { CreateIngredient } from 'src/app/models/product/createIngredient';
import { RecipesHelperService } from 'src/app/core/services/recipe/recipesHelper/recipes-helper.service';
import { RecipeService } from 'src/app/core/services/recipe/recipe.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificatorService } from 'src/app/core/services/notificator/notificator.service';
import { FoodGroup } from 'src/app/models/product/food-group';
import { SingleProductViewComponent } from '../single-product-view/single-product-view.component';
import { Recipe } from 'src/app/models/recipe/recipe';
import { RecipeNutritionInfo } from 'src/app/models/recipe/recipeSmallNutritionInfo';
import { Subscription } from 'rxjs';
import { ShowAllRecipes } from 'src/app/models/recipe/showAllRecipes';

@Component({
  selector: 'app-create-recipe-view',
  templateUrl: './create-recipe-view.component.html',
  styleUrls: ['./create-recipe-view.component.css']
})
export class CreateRecipeViewComponent implements OnInit, OnDestroy {

  public toggled = false;
  public totalItems: number;
  public foodGroups: FoodGroup[] = [];
  public products: Product[] = [];
  public ingredients: Ingredient[] = [];
  public recipeFormGroup: FormGroup;
  public recipeName: AbstractControl;
  public recipeType: AbstractControl;
  public ingredientsToCreateRecipe: CreateIngredient[] = [];
  public nutritionInfo: RecipeNutritionInfo;
  public subRecipes: Recipe[] = [];
  public ingredientToCalc: Ingredient[] = [];
  public routeSubscription: Subscription;
  public updateIsActive: boolean;
  public idOfRecipeToUpd: string;
  public recipeToUpd: ShowAllRecipes;

  constructor(
    private readonly productsService: ProductsService,
    private readonly modalService: NgbModal,
    private readonly productsHelperService: ProductsHelperService,
    private readonly formBuilder: FormBuilder,
    private readonly recipeHelperService: RecipesHelperService,
    private readonly recipeService: RecipeService,
    private readonly router: Router,
    private readonly notificator: NotificatorService,
    private readonly activatedRoute: ActivatedRoute,
  ) { }


  ngOnInit() {
    this.productsService.getProducts().subscribe((data: { items: Product[] }) => {
      this.products = data.items;
    });
    this.productsService.getAllProductsNumber().subscribe((count: number) => {
      this.totalItems = count;
    });
    this.productsService.getAllFoodGroups().subscribe((data: FoodGroup[]) => {
      this.foodGroups = data;
    });
    this.recipeFormGroup = this.formBuilder.group({
      recipeName: ['', [Validators.required, Validators.minLength(3)]],
      recipeType: new FormControl(),
    });
    this.recipeName = this.recipeFormGroup.controls.recipeName;
    this.recipeType = this.recipeFormGroup.controls.recipeType;

    this.routeSubscription = this.activatedRoute.params.subscribe(params => {

      if (params.recipeType === 'meal' && params.recipeType) {
        this.recipeService.getSingleMeal(params.id).subscribe((data: any) => {
          this.idOfRecipeToUpd = params.id;
          this.updateIsActive = true;
          this.recipeName.setValue(data.title);
          this.recipeType.setValue(data.type);
          this.subRecipes.push(...data.subRecipes);
          this.ingredients.push(...data.ingredients);
          this.ingredientToCalc.push(...data.ingredients);
          this.nutritionInfo = this.recipeHelperService.calculateRecipeNutritions(data.ingredients);
          this.ingredientsToCreateRecipe.push(...this.productsHelperService.convertIngredients(data.ingredients));
        }, () => console.log('big error !'));
      } else if (params.recipeType) {
        this.recipeService.getSingleRecipe(params.id).subscribe(data => {
          const complexRecipes = data.parentRecipes.filter(r => r !== null);
          if (complexRecipes.length > 0) {
            const parentRecipes = complexRecipes.join(', ');
            this.notificator.warn(`Updating this will affect: ${parentRecipes}`);
          }
          this.idOfRecipeToUpd = params.id;
          this.updateIsActive = true;
          this.recipeName.setValue(data.title);
          this.recipeType.setValue(data.type);
          this.ingredients.push(...data.ingredients);
          this.ingredientToCalc.push(...data.ingredients);
          this.nutritionInfo = this.recipeHelperService.calculateRecipeNutritions(data.ingredients);
          this.ingredientsToCreateRecipe.push(...this.productsHelperService.convertIngredients(data.ingredients));
        }, () => console.log('big error !'));
      }
    });
  }

  public openProductsModal() {
    const modalRef = this.modalService.open(AllProductsViewComponent, { size: 'lg' });
    modalRef.componentInstance.products = this.products;
    modalRef.componentInstance.collectionSize = this.totalItems;
    modalRef.componentInstance.foodGroups = this.foodGroups;

    modalRef.result.then(result => {

      this.productsService.getASingleProduct(result.productCode).subscribe(data => {
        const productToAdd = data;
        const ingredient = this.productsHelperService.createIngredients(productToAdd, result.amount, result.measure);
        this.ingredients.push(ingredient);
        this.ingredientToCalc.push(ingredient);
        const addIngredientToRecipe: CreateIngredient = {
          productCode: productToAdd.code,
          amount: result.amount,
        };
        this.ingredientsToCreateRecipe.push(addIngredientToRecipe);
        this.nutritionInfo = this.recipeHelperService.calculateRecipeNutritions(this.ingredientToCalc);
        this.notificator.success('Ingredient added !');
      });
    }).catch(() => console.log('Nothing selected'));
  }
  public CreateRecipe(recipeName: string, recipeType: string) {
    if (this.subRecipes.length > 0) {
      const complexRecipeToCreate = this.recipeHelperService
        .createComplexRecipe(recipeName, this.subRecipes, this.ingredientsToCreateRecipe);
      this.recipeService.createComplexRecipe(complexRecipeToCreate).subscribe(data => {
        this.notificator.success('Recipe Created !');
        this.router.navigate(['/recipes']);
      });
    } else {
      const recipeToCreate = this.recipeHelperService.
        createRecipeTransform(recipeName, recipeType, this.ingredientsToCreateRecipe);
      this.recipeService.createRecipe(recipeToCreate).subscribe(() => {
        this.notificator.success('Recipe Created !');
        this.router.navigate([`recipes`]);
      }, (error) => this.notificator.error(error.error.message));
    }
  }
  public deleteIngredient(ingredient: Ingredient) {
    const ingredientId = this.ingredients.findIndex(ing => ing === ingredient);
    const idOfCalcIngr = this.ingredientToCalc.findIndex(ing => ing === ingredient);
    const idOfIngr = this.ingredientsToCreateRecipe.findIndex(ingr => ingr.productCode === ingredient.productCode);
    this.notificator.error('Ingredient Removed !');
    this.ingredientsToCreateRecipe.splice(idOfIngr, 1);
    this.ingredients.splice(ingredientId, 1);
    this.ingredientToCalc.splice(idOfCalcIngr, 1);
    this.nutritionInfo = this.recipeHelperService.calculateRecipeNutritions(this.ingredientToCalc);
  }
  public updateIngredient(ingredientToUpd: Ingredient) {
    this.productsService.getASingleProduct(ingredientToUpd.productCode).subscribe(data => {
      const modalRef = this.modalService.open(SingleProductViewComponent);
      modalRef.componentInstance.productCode = data.code;
      modalRef.componentInstance.measure = data.measures;
      modalRef.result.then(result => {
        const ingredient = this.productsHelperService.createIngredients(data, result.amount, result.measure);
        const idOfingredientToSplice = this.ingredients.findIndex(ing => ing === ingredientToUpd);
        const ifOfCalcIngr = this.ingredientToCalc.findIndex(ing => ing === ingredientToUpd);
        const idOfCreateIngr = this.ingredientsToCreateRecipe.findIndex(ing => ing.productCode === ingredient.productCode);
        this.ingredients.splice(idOfingredientToSplice, 1, ingredient);
        this.ingredientToCalc.splice(ifOfCalcIngr, 1, ingredient);
        const addIngredientToRecipe: CreateIngredient = {
          productCode: data.code,
          amount: result.amount,
        };
        this.ingredientsToCreateRecipe.splice(idOfCreateIngr, 1);
        this.ingredientsToCreateRecipe.push(addIngredientToRecipe);
        this.nutritionInfo = this.recipeHelperService.calculateRecipeNutritions(this.ingredientToCalc);
        this.notificator.success('Ingredient successfuly updated!');
      });
    });
  }
  public toggle(): void {
    this.toggled = !this.toggled;
  }

  public addRecipe(recipe: Recipe) {
    this.toggle();
    const recipeToAdd = this.subRecipes.find(r => r.title === recipe.title);
    if (recipeToAdd) {
      this.notificator.error('This recipe is already added');
    } else {
      this.subRecipes.push(recipe);
      this.ingredientToCalc.push(...recipe.ingredients);
      this.nutritionInfo = this.recipeHelperService.calculateRecipeNutritions(this.ingredientToCalc);
      this.notificator.success('Recipe added!');
    }
  }
  public deleteRecipe(recipe: Recipe) {
    const idOfsubRecipe = this.subRecipes.findIndex(r => r === recipe);
    this.subRecipes.splice(idOfsubRecipe, 1);
    this.ingredientToCalc = this.ingredientToCalc.filter(ingr => {
      return !recipe.ingredients.includes(ingr);
    });
    this.nutritionInfo = this.recipeHelperService.calculateRecipeNutritions(this.ingredientToCalc);
    this.notificator.error('Recipe removed!');
  }
  public updateRecipe(recipeName: string, recipeType: string) {
    if (recipeType === 'meal') {
      this.updateComplexRecipe(recipeName, recipeType);
    } else {
      const recipeToCreate = this.recipeHelperService.
        createRecipeTransform(recipeName, recipeType, this.ingredientsToCreateRecipe);
      this.recipeService.updateRecipe(this.idOfRecipeToUpd, recipeType, recipeToCreate).subscribe(() => {
        this.notificator.success('Success update !');
        this.router.navigate([`recipes/${this.idOfRecipeToUpd}/${recipeType}`]);
      }, (err) => this.notificator.error(err.error.message));
    }
  }
  public updateComplexRecipe(recipeName: string, recipeType: string) {
    const complexRecipeToCreate = this.recipeHelperService
      .createComplexRecipe(recipeName, this.subRecipes, this.ingredientsToCreateRecipe);
    this.recipeService.updateMeal(this.idOfRecipeToUpd, complexRecipeToCreate).subscribe(data => {
      this.notificator.success('Success update !');
      this.router.navigate([`/recipes/${this.idOfRecipeToUpd}/${recipeType}`]);
    });
  }
  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
  }
}
