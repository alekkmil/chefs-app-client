import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { CreateRecipeViewComponent } from './create-recipe-view.component';
import { of } from 'rxjs';
import { NotificatorService } from 'src/app/core/services/notificator/notificator.service';
import { ProductsService } from 'src/app/core/services/products/products.service';
import { ProductsHelperService } from 'src/app/core/services/products/productsHelper/products-helper.service';
import { RecipeService } from 'src/app/core/services/recipe/recipe.service';
import { RecipesHelperService } from 'src/app/core/services/recipe/recipesHelper/recipes-helper.service';
import { Nutrient } from 'src/app/models/product/nutrient';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

describe('CreateRecipeViewComponent', () => {
  let component: CreateRecipeViewComponent;
  let fixture: ComponentFixture<CreateRecipeViewComponent>;

  let httpClient: HttpClient;

  let httpTestingController: HttpTestingController;

  const notificator = jasmine.createSpyObj('NotificatorService', ['success', 'error']);

  const productsHelperService = jasmine.createSpyObj('ProductsHelperService', ['convertIngredients', 'createIngredients']);

  const recipesHelperService = jasmine
    .createSpyObj('RecipesHelperService', ['calculateRecipeNutritions', 'createComplexRecipe', 'createRecipeTransform']);

  const productsService = {
    getProducts: () => of([
      {
        code: 123,
        description: '',
        measures: [],
        nutrition: {}
      },
      {
        code: 124,
      },
    ]),
    getASingleProduct: () => of({
      code: 123
    }),
    getAllFoodGroups: () => of([
      {
        code: 1,
      },
      {
        code: 2,
      }
    ]),
    getAllProductsNumber: () => of(2),
  };

  const recipeService = {
    getSingleMeal: () => {
      return of({
        id: '1',
        image: '',
        title: 'title',
        type: 'type',
        ingredients: [
          ingredient
        ],
        recipeNutrition: {
          PROCNT: nutrient,
          FAT: nutrient,
          CHOCDF: nutrient,
          ENERC_KCAL: nutrient,
          NA: nutrient,
          FE: nutrient,
          SUGAR: nutrient,
          FIBTG: nutrient,
          P: nutrient,
          VITD: nutrient
        },
        subRecipies: []

      });
    },
    getSingleRecipe: () => {
      return of({
        id: '1',
        image: '',
        title: 'title',
        type: 'type',
        ingredients: [
          ingredient
        ],
        recipeNutrition: {
          PROCNT: nutrient,
          FAT: nutrient,
          CHOCDF: nutrient,
          ENERC_KCAL: nutrient,
          NA: nutrient,
          FE: nutrient,
          SUGAR: nutrient,
          FIBTG: nutrient,
          P: nutrient,
          VITD: nutrient
        },
        parentRecipes: []
      });
    },
    createComplexRecipe: () => of({}),
    createRecipe: () => of({}),
    updateRecipe: () => of({}),
    updateMeal: () => of({}),
  };

  let route = {
    params: of({
      id: '1',
      recipeType: 'notMeal'
    })
  };
  const router = jasmine.createSpyObj('Router', ['navigate']);

  // data
  const nutrient: Nutrient = {
    description: '',
    unit: '2',
    value: 2
  };

  const ingredient = {
    productCode: 1,
    amount: 1,
    measure: 'measure',
    nutrients: [nutrient, nutrient],
    productDescription: 'desc',
  };

  const recipe = {
    id: '1',
    image: '',
    title: 'title',
    type: 'type',
    ingredients: [
      ingredient
    ],
    recipeNutrition: {
      PROCNT: nutrient,
      FAT: nutrient,
      CHOCDF: nutrient,
      ENERC_KCAL: nutrient,
      NA: nutrient,
      FE: nutrient,
      SUGAR: nutrient,
      FIBTG: nutrient,
      P: nutrient,
      VITD: nutrient
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, ReactiveFormsModule],
      declarations: [CreateRecipeViewComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: route
        },
        {
          provide: Router, useValue: router
        },
        {
          provide: NotificatorService, useValue: notificator
        },
        {
          provide: ProductsService, useValue: productsService
        },
        {
          provide: ProductsHelperService, useValue: productsHelperService
        },
        {
          provide: RecipeService, useValue: recipeService
        },
        {
          provide: RecipesHelperService, useValue: recipesHelperService
        },
        {
          provide: FormBuilder, useValue: new FormBuilder()
        },

      ]
    })
      .compileComponents();
  }));

  describe('OnInit tests', () => {

    it('OnInit should initialize correct values', () => {

      productsService.getProducts()
        .subscribe(result =>
          expect(component.products.length).toEqual(result.length));

      productsService.getAllFoodGroups()
        .subscribe(result =>
          expect(component.foodGroups.length).toEqual(result.length));

      productsService.getAllProductsNumber()
        .subscribe(result =>
          expect(component.totalItems).toEqual(result));

    });
  });

  beforeEach(() => {
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    fixture = TestBed.createComponent(CreateRecipeViewComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('CreateRecipe should create recipe when no subRecipies', () => {
    recipesHelperService.createRecipeTransform.and.returnValue('recipe');

    component.CreateRecipe('name', 'type');
    expect(router.navigate).toHaveBeenCalledTimes(1);
    router.navigate.calls.reset();
  });

  it('CreateRecipe should create complex recipe when more than one subRecipies', () => {
    component.subRecipes = [recipe];

    recipesHelperService.createRecipeTransform.and.returnValue('recipe');

    component.CreateRecipe('name', 'type');
    expect(router.navigate).toHaveBeenCalledTimes(1);
    router.navigate.calls.reset();
  });

  it('deleteIngredient should succeed', () => {
    const currentLength = component.ingredients.length;
    const expectedLength = currentLength - 1;

    component.deleteIngredient(ingredient);

    expect(component.ingredients.length).toEqual(expectedLength);
    expect(component.ingredientToCalc.length).toEqual(expectedLength);
    expect(component.ingredientsToCreateRecipe.length).toEqual(expectedLength);
  });

  it('updateIngredient should succeed', () => {
    // TODO: mock/create modalService and write test
    expect(1).toEqual(1);
  });

  it('toggle should switch toggled', () => {
    component.toggled = false;
    const expected = true;

    component.toggle();

    expect(component.toggled).toEqual(expected);
  });

  it('addRecipe should add recipe', () => {
    const expectedLength = component.subRecipes.length + 1;

    recipesHelperService.calculateRecipeNutritions.and.returnValue({});

    component.addRecipe(recipe);

    expect(component.subRecipes.length).toEqual(expectedLength);
  });
  it('addRecipe should add recipe ingredients', () => {
    const expectedLength = component.ingredientToCalc.length + recipe.ingredients.length;

    recipesHelperService.calculateRecipeNutritions.and.returnValue({});

    component.addRecipe(recipe);

    expect(component.ingredientToCalc.length).toEqual(expectedLength);
  });

  it('deleteRecipe should remove recipe', () => {
    const expectedLength = Math.min(component.subRecipes.length - 1, 0);

    recipesHelperService.calculateRecipeNutritions.and.returnValue({});

    component.deleteRecipe(recipe);

    expect(component.subRecipes.length).toEqual(expectedLength);
  });

  it('delete should remove recipe ingredients', () => {
    const expectedLength = Math.min(component.ingredientToCalc.length - recipe.ingredients.length, 0);

    recipesHelperService.calculateRecipeNutritions.and.returnValue({});

    component.deleteRecipe(recipe);

    expect(component.ingredientToCalc.length).toEqual(expectedLength);
  });

  it('updateRecipe should navigate away when recipeType is not a meal', () => {

    recipesHelperService.createRecipeTransform.and.returnValue({});

    component.updateRecipe('name', 'type');
    expect(router.navigate).toHaveBeenCalledTimes(1);
    router.navigate.calls.reset();
  });

  it('updateRecipe should navigate away when recipeType is a meal', () => {
    route = {
      params: of({
        id: '1',
        recipeType: 'meal'
      })
    };
    recipesHelperService.createRecipeTransform.and.returnValue({});

    component.updateRecipe('name', 'type');
    expect(router.navigate).toHaveBeenCalledTimes(1);
    router.navigate.calls.reset();
  });

  it('updateComplexRecipe should navigate away', () => {

    recipesHelperService.createComplexRecipe.and.returnValue({});

    component.updateComplexRecipe('name', 'type');
    expect(router.navigate).toHaveBeenCalledTimes(1);
    router.navigate.calls.reset();
  });

  afterEach(() => {
    if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
      (fixture.nativeElement as HTMLElement).remove();
    }
  });
});
