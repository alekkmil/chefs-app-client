import { Component, OnInit, Input } from '@angular/core';
import { Measure } from 'src/app/models/product/measure';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-single-product-view',
  templateUrl: './single-product-view.component.html',
  styleUrls: ['./single-product-view.component.css']
})
export class SingleProductViewComponent implements OnInit {

  @Input() public measure: Measure[];
  @Input() public productCode: Measure[];
  public measureToPrint: Measure[] = [];

  constructor(
    public activeModal: NgbActiveModal,
  ) { }

  ngOnInit() {
    this.measureToPrint = JSON.parse(JSON.stringify(this.measure));
    console.log(this.measureToPrint);
  }
  addIngredient(amount: number , measure: string) {
    const resultToReturn = {
      amount,
      productCode: this.productCode,
      measure,
    };
    this.activeModal.close(resultToReturn);
  }
  log(measure: string, quantity: number) {

    const measureToChange = this.measure.find(m => m.id === measure);
    const measureToPrint = this.measureToPrint.find(m => m.id === measure);

    measureToPrint.gramsPerMeasure = measureToChange.gramsPerMeasure * quantity;
  }
}
