
import { FoodGroup } from './../../models/product/food-group';

import { Product } from './../../models/product/product';
import { Component, OnInit, Input } from '@angular/core';
import { ProductsService } from './../../core/services/products/products.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { QueryProducts } from 'src/app/models/product/query-products';
import { Measure } from 'src/app/models/product/measure';
import { SingleProductViewComponent } from '../single-product-view/single-product-view.component';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-all-products-view',
  templateUrl: './all-products-view.component.html',
  styleUrls: ['./all-products-view.component.css']
})
export class AllProductsViewComponent implements OnInit {


  products: Product[];
  collectionSize: number;
  page = 1;
  pageSize = 10;
  foodGroups: FoodGroup[];
  selectedFoodGroup: FoodGroup;

  productForm: FormGroup = new FormGroup({
    // foodGroup: new FormControl(),
    searchField: new FormControl(''),
  });

  constructor(
    private readonly productsService: ProductsService,
    public activeModal: NgbActiveModal,
    private readonly modalService: NgbModal,
  ) { }

  ngOnInit() {
    this.productForm.get('searchField').valueChanges.subscribe((val) => {
      if (val === '' || val.length > 3) {
        this.page = 1;
        this.pageChanged(1);
      }
    });
  }

  public openSingleProductView(measure: Measure, productCode: number) {
    const modalRef = this.modalService.open(SingleProductViewComponent);
    modalRef.componentInstance.measure = measure;
    modalRef.componentInstance.productCode = productCode;

    modalRef.result.then(result => {
      this.activeModal.close(result);
    }).catch(() => console.log('nothing selected'));
  }

  public pageChanged(event: any): void {
    let foodGr = 0;
    if (this.selectedFoodGroup) {
      foodGr = this.selectedFoodGroup.foodcode;
    }
    this.productsService.getQueryProducts(event, this.productForm.value.searchField, foodGr)
      .subscribe((data: QueryProducts) => {
        this.products = data.products;
        this.collectionSize = data.totalProducts;
      });
    }

  public filterProductsByFoodGroup(foodGroup: FoodGroup): void {
    this.selectedFoodGroup = foodGroup;
    this.page = 1;
    this.pageChanged(1);
  }

  public getAllProducts(): void {
    this.selectedFoodGroup = null;
    this.page = 1;
    this.pageChanged(1);
  }

  public clearFilters() {
    // tslint:disable-next-line:no-string-literal
    this.productForm.controls['searchField'].setValue('');
    this.selectedFoodGroup = null;
    this.page = 1;
    this.pageChanged(1);
  }
}
