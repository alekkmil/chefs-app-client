import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleRecipeViewComponent } from './single-recipe-view.component';

describe('SingleRecipeViewComponent', () => {
  let component: SingleRecipeViewComponent;
  let fixture: ComponentFixture<SingleRecipeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleRecipeViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleRecipeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
