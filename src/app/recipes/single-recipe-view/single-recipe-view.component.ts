import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Recipe } from 'src/app/models/recipe/recipe';
import { RecipeService } from 'src/app/core/services/recipe/recipe.service';
import { RecipesHelperService } from 'src/app/core/services/recipe/recipesHelper/recipes-helper.service';
import { Nutrition } from 'src/app/models/product/nutrition';
import { Nutrient } from 'src/app/models/product/nutrient';
import { ShowAllRecipes } from 'src/app/models/recipe/showAllRecipes';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmModalComponent } from 'src/app/shared/confirm-modal/confirm-modal.component';
import { NotificatorService } from 'src/app/core/services/notificator/notificator.service';

@Component({
  selector: 'app-single-recipe-view',
  templateUrl: './single-recipe-view.component.html',
  styleUrls: ['./single-recipe-view.component.css']
})
export class SingleRecipeViewComponent implements OnInit, OnDestroy {

  public routeParamsSubscription: Subscription;
  public recipe: any;
  public recipeId: string;
  public ingredientClicked: boolean;
  public ingredientToShow: string;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly recipesService: RecipeService,
    private readonly recipeHelperService: RecipesHelperService,
    private readonly modalService: NgbModal,
    private readonly router: Router,
    private readonly notificator: NotificatorService,
  ) { }

  ngOnInit() {
    this.routeParamsSubscription = this.activatedRoute.params.subscribe((params) => {
      if (params.recipeType === 'meal') {
        this.recipeId = params.id;
        this.recipesService.getSingleMeal(this.recipeId).subscribe(data => {
          this.recipe = data;
          this.recipe = this.recipeHelperService.appendImage([this.recipe])[0];
        });
      } else {
        this.recipeId = params.id;
        this.recipesService.getSingleRecipe(this.recipeId).subscribe(data => {
          this.recipe = data;
          this.recipe = this.recipeHelperService.appendImage([this.recipe])[0];
        });
      }
    });
  }
  ngOnDestroy() {
    this.routeParamsSubscription.unsubscribe();
  }
  public openSubRecipe(recipeId: string, recipeType: string) {
    this.router.navigate([`recipes/${recipeId}/${recipeType}`]);
  }

  public toggle(name: string) {
    this.ingredientToShow = name;
    this.ingredientClicked = !this.ingredientClicked;
  }

  public deleteRecipe(recipeId: string, recipeType: string) {
    const modalRef = this.modalService.open(ConfirmModalComponent);
    modalRef.componentInstance.id = recipeId;
    modalRef.componentInstance.recipeType = recipeType;
    modalRef.result.then( id => this.recipesService.deleteRecipe(id, recipeType).subscribe( () => {
      this.router.navigate(['recipes']);
      this.notificator.success('The recipe is gone !');
    })).catch( () => console.log('canceled'));
  }
  public updateRecipe(recipeId: string, recipeType: string) {
    this.router.navigate([`recipes/update/${recipeType}/${recipeId}`]);
  }
}
