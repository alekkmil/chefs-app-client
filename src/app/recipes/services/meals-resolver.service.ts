import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { NotificatorService } from 'src/app/core/services/notificator/notificator.service';
import { RecipeService } from 'src/app/core/services/recipe/recipe.service';
import { ShowAllRecipes } from 'src/app/models/recipe/showAllRecipes';

@Injectable({
  providedIn: 'root'
})
export class MealsServiceResolver implements Resolve<{meals: ShowAllRecipes[]}> {
   username: string;
  constructor(
    private readonly recipesService: RecipeService,
    private readonly notificator: NotificatorService,
    ) {
    }

  public resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ) {
    return this.recipesService.getAllMeals()
      .pipe(catchError(
        res => {
          this.notificator.error(res.error.error);
          // Alternativle, if the res.error.code === 401, you can logout the user and redirect to /home
          return of({meals: []});
        }
      ));
  }
}
