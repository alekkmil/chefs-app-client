import { NgModule } from '@angular/core';
import { AllRecipesViewComponent } from './all-recipes-view/all-recipes-view.component';
import { SingleRecipeViewComponent } from './single-recipe-view/single-recipe-view.component';
import { RecipesComponent } from './recipes.component';
import { RecipesRoutingModule } from './recipes-routing.module';
import { CommonModule } from '@angular/common';
import { CreateRecipeViewComponent } from './create-recipe-view/create-recipe-view.component';
import { AllProductsViewComponent } from './all-products-view/all-products-view.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import { Ng5SliderModule } from 'ng5-slider';

@NgModule({
  declarations: [
    AllRecipesViewComponent,
    SingleRecipeViewComponent,
    RecipesComponent,
    CreateRecipeViewComponent,
    AllProductsViewComponent,
  ],
  imports: [
    CommonModule,
    RecipesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgbPaginationModule,
    Ng5SliderModule,
     ],
     entryComponents: [AllProductsViewComponent]
 })
export class RecipesModule { }
