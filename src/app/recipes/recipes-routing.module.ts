import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { RecipesComponent } from './recipes.component';
import { CreateRecipeViewComponent } from './create-recipe-view/create-recipe-view.component';
import { SingleRecipeViewComponent } from './single-recipe-view/single-recipe-view.component';


const routes: Routes = [
    { path: '', component: RecipesComponent, pathMatch: 'full' },
    { path: 'create' , component: CreateRecipeViewComponent },
    { path: 'update/:recipeType/:id' , component: CreateRecipeViewComponent },
    { path: ':id/:recipeType' , component: SingleRecipeViewComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecipesRoutingModule {}
