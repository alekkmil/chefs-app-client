import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllRecipesViewComponent } from './all-recipes-view.component';

describe('AllRecipesViewComponent', () => {
  let component: AllRecipesViewComponent;
  let fixture: ComponentFixture<AllRecipesViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllRecipesViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllRecipesViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
