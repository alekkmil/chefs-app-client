import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Product } from 'src/app/models/product/product';
import { Recipe } from 'src/app/models/recipe/recipe';
import { RecipesHelperService } from 'src/app/core/services/recipe/recipesHelper/recipes-helper.service';
import { Router } from '@angular/router';
import { ShowAllRecipes } from 'src/app/models/recipe/showAllRecipes';

@Component({
  selector: 'app-all-recipes-view',
  templateUrl: './all-recipes-view.component.html',
  styleUrls: ['./all-recipes-view.component.css']
})
export class AllRecipesViewComponent implements OnInit {

  @Input() public recipes: ShowAllRecipes[] = [];
  @Input() public isCreateRecipeActive;
  @Input() public subRecipes: Recipe[] = [];
  @Output() public addRecipe = new EventEmitter();

  constructor(
    private readonly router: Router,
  ) { }

  ngOnInit() {
  }


  public openSingleRecipe(recipeId: string, recipeType: string) {
    this.router.navigate([`recipes/${recipeId}/${recipeType}`]);
  }

  public create(recipe: Recipe) {
    this.addRecipe.emit(recipe);
  }
}
