import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Recipe } from '../models/recipe/recipe';
import { RecipeService } from '../core/services/recipe/recipe.service';
import { RecipesHelperService } from '../core/services/recipe/recipesHelper/recipes-helper.service';
import { ShowAllRecipes } from '../models/recipe/showAllRecipes';
import { Options, LabelType } from 'ng5-slider';
import { HighestValues } from '../models/recipe/highestValues';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit {

  public lowerInput = 0;
  public higherInput: number;
  public highestValues: HighestValues;
  public recipes: ShowAllRecipes[] = [];
  public recipesToPassDown: ShowAllRecipes[] = [];
  public selectRecipe = false;
  public filteredRecipes: ShowAllRecipes[] = [];
  public options: Options;
  public nutrientToSearch = 'ENERC_KCAL';
  public recipeGroup = 'All Recipes';
  public recipesToFilter: ShowAllRecipes[] = [];

  @Input() isCreateRecipeActive: boolean;
  @Input() subRecipes: Recipe[];
  @Output() addRecipe = new EventEmitter();

  constructor(
    private readonly recipesHelperService: RecipesHelperService,
    private readonly route: ActivatedRoute,
    private readonly recipesService: RecipeService,
  ) { }

  ngOnInit() {
    this.route.data.subscribe((data: any) => {
      this.recipes = data.recipes;
      this.recipesToPassDown = this.recipesHelperService.appendImage(this.recipes);
      this.route.data.subscribe((meals: any) => {
        const mealsWithImages = this.recipesHelperService.appendImage(meals.meals);
        this.recipesToPassDown.push(...mealsWithImages);
        this.recipesToFilter.push(...this.recipesToPassDown);
        this.filteredRecipes = this.recipesToPassDown.sort((a, b) => {
          return  (new Date(b.date) as any) - (new Date(a.date) as any);
        });

        if (this.recipesToPassDown.length > 0) {
          this.highestValues = this.recipesHelperService.getMaxNutrValues(this.recipesToPassDown);
          this.higherInput = this.highestValues.ENERC_KCAL;
          this.options = {
            floor: 0,
            ceil: this.higherInput,
          };
        }
      });
    });
  }

  public filter() {
    this.filteredRecipes = this.recipesToFilter.filter(x =>
      x.recipeNutrition[this.nutrientToSearch].value >= this.lowerInput - 1 &&
      x.recipeNutrition[this.nutrientToSearch].value <= this.higherInput + 1
    );
  }
  public changeNutrient(nutrient: string) {
    this.nutrientToSearch = nutrient;
    this.lowerInput = 0;
    this.higherInput = this.highestValues[nutrient];
    this.options = {
      floor: 0,
      ceil: this.higherInput,
    };
    this.filteredRecipes = this.recipesToFilter;
  }

  public clearFilters() {
    this.recipeGroup = 'All Recipes';
    this.highestValues = this.recipesHelperService.getMaxNutrValues(this.recipesToPassDown);
    this.higherInput = this.highestValues.ENERC_KCAL;
    this.lowerInput = 0;
    this.nutrientToSearch = 'ENERC_KCAL';
    this.options = {
      floor: 0,
      ceil: this.higherInput,
    };
    this.filteredRecipes = this.recipesToPassDown;
    this.recipesToFilter = this.recipesToPassDown;
  }

  public create(recipe: Recipe) {
    this.isCreateRecipeActive = !this.isCreateRecipeActive;
    this.addRecipe.emit(recipe);
  }

  public changeGroup() {
    if (this.recipeGroup === 'All Recipes') {
      this.filteredRecipes = this.recipesToPassDown;
      this.recipesToFilter = this.recipesToPassDown;
      this.highestValues = this.recipesHelperService.getMaxNutrValues(this.recipesToPassDown);
      this.higherInput = this.highestValues[this.nutrientToSearch];
      this.lowerInput = 0;
      this.options = {
        floor: 0,
        ceil: this.higherInput,
      };
    } else {
      this.recipesToFilter = this.recipesToPassDown.filter(recipe => recipe.type === this.recipeGroup);
      this.filteredRecipes = this.recipesToFilter;
      if (this.recipesToFilter.length > 0) {
        this.highestValues = this.recipesHelperService.getMaxNutrValues(this.recipesToFilter);
        this.higherInput = this.highestValues[this.nutrientToSearch];
        this.lowerInput = 0;
        this.options = {
          floor: 0,
          ceil: this.higherInput,
        };
      }
    }
  }

}
