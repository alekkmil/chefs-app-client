# ChefsApp

For more information you can open the index.html file in the app-documentation folder !

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Running the app

1. git clone https://gitlab.com/alekkmil/chefs-app-client.git
2. when in the clone directory run npm install
3. ng server (if the API is running you should be good to go ... for info on how to run the API go to
https://gitlab.com/alekkmil/chefs-app-api )
4. you can use username: aleks password: 1234 for already preloaded recipes otherwise register !

## Further help
For more information download the app documenation !
Feel free to email me on alekkmilanov@mail.bg

